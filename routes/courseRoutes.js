const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require("../auth");

// Route for creating a course
/*router.post("/", (req, res) => {
	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
});*/

// refactor this route to implement user authentication for our admin when creating a course
/*router.post("/", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
});*/

router.post("/", auth.verify, (req, res) => {

    const data = {
        course: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    };
    // = data.course.name
    console.log(data);
    courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving all the courses
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});


// Route for retrieving all active courses
router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});


// Route for retrieving a specific course
router.get("/:courseId", (req, res) => {
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});


// Route for Updating a Course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send (resultFromController));
});


// Route for Archiving a Course
router.put("/:courseId/archive", auth.verify, (req, res) => {
	courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send (resultFromController));
});


// SOLUTION: Route for Archiving a Course
/*router.put("/:courseId/archive", auth.verify, (req, res) => {
	courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController));
});*/



module.exports = router;