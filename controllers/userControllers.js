// import the user model
const User = require("../models/User");
const Course = require("../models/Course");

// import bcrypt
const bcrypt = require("bcrypt");
// bcyrpt is a package which allows us to hash our passwords to add layer of security for our user's details

// import auth
const auth = require("../auth");



// Check if the email already exists
/*
	STEPS
	1. Use mongoose "find" method to find duplicates emails
	2. Use the "then" method to send a response back to the FE application based on the result of the "find" method
*/

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email:reqBody.email}).then(result =>{
		if(result.length>0) {
			return true;
		} else {
			return false;
		}
	});
};


// User Registration
/*
	STEPS:
	1. Create a new User object using the mongoose models and the info from the reqBody
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/
module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password,10)
	});

	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	});
};


// User Authentication
/*
	STEPS:
	1. Check the database if the user email exists
	2. Compare the password in the login form with the password stored in the database
	3. Generate/return a JSON Web Token if the user is successfully login in and return false if not
*/
module.exports.loginUser = (reqBody) => {
	return User.findOne({email:reqBody.email}).then(result => {
		if (result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect) {
				return { access: auth.createAccessToken(result)}
			} else {
				return false;
			}
		}
	});
};


/***************** ACTIVITY 38  *****************/

// User Details
/*
	STEPS:
	1. Find the document in the database using the user's ID
	2. Reassign password of the returned document to an empty string
	3. Return the result back to the frontend
*/
/*
module.exports.getProfile = (userId) => {
  return User.findById(userId).then((user, error) => {
    if (error) {
      console.log(error);
      return false;
    } else {
      if (user !== null) {
        user.password = "";
        return user;
      } else {
        return false;
      }
    };
  })
};*/


// SOLUTION #1
// Retrieve user Details
/*
	STEPS
	1. Find the document in the database using the user's ID
	2. Reassign password of the returned document to an empty string
	3. Return the result back to the frontend
*/

/*module.exports.getProfile = (reqBody) => {
	return User.findById(reqBody.id).then(result => {
		result.password = "";
		return result;
	});
};*/


// SOLUTION #2
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
			result.password = "";
			return result;
	})
};


//Enroll user to a class
/*
	Steps:
	1.Find the document in the database using the user's ID
	2.Add the courseID to the user's enrollement array
	3. Update the doucmnet in MongoDB

*/

/*
	First, find the user who is enrolling and update his enrollments subdocumnet array. We will push the courseID in the enrollments array

	Second, find the course where we are enrolling and update its enrollees subdocument array. We will push the userId in the enrollees array.

	Since we will access 2 collections in one action -- we will have to wait for the completion of the action instead of letting Javascript continue line per line
	
	To be able to wait for the result of a function, we use the await keyword. The await keyword allows us to wait for the function to finish and get a result before proceeding

*/

module.exports.enroll = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.enrollments.push({courseId:data.courseId});

		return user.save().then((user,error)=>{
			if (error) {
				return false;
			}
			else {
				return true;
			};
		});

	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

		course.enrollees.push({userId:data.userId});

		return course.save().then((course,error) => {
			if (error) {
				return false;
			}
			else {
				return true;
			};
		});
	});

	if(isUserUpdated && isCourseUpdated) {
		return true;
	}
	else {
		return false;
	};
};
