const Course = require("../models/Course");


/***************** MINI ACTIVITY  *****************/ 
//Create a new course
/*
	STEP:
	1. Create a new Course object using the mongoose model and the information from the reqBody and (the id from the header)
	2. Save the new User to the database
*/

/*module.exports.addCourse = (reqBody) => {
	let addCourse = new Course ({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price,
		slots : reqBody.slots,
		createdOn : reqBody.createdOn,
		enrollees: reqBody.enrollees
	});

	return newCourse.save().then((course, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	});
};*/

/***************** SOLUTION  *****************/ 
/*module.exports.addCourse = (reqBody) => {
	let newCourse = new Course ({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price,
	});

	return newCourse.save().then((course, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	});
};
*/


/***************** ACTIVITY 39  *****************/ 
// refactor the addCourse controller method to implement admin authentication for creating course

/*module.exports.addCourse = (data) => {
    let newCourse = new Course (data.course);

    if (data.isAdmin)
    return newCourse.save().then((result, error) => {
    	return true;
    });
};
*/

/***************** ACTIVITY 39 SOLUTION  *****************/ 
/*
	STEPS
	1. Create a conditional statement that will check if the user is an admin
	2. Create a new Course object using mongoose model and the information the reqBody and the id from the header
	3. Save the new course to the database
*/
// Solution 1
/*module.exports.addCourse = (reqBody) => {
	let newCourse = new Course ({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	});

	if (reqBody.isAdmin) {
		return newCourse.save().then((course, error) => {
			if (error) {
				return false;
			}
			else {
				return true;
			}
		});
	}
	else {
		return false;
	}
};
*/
// Solution 2
module.exports.addCourse = (data) => {

	console.log(data);

	if (data.isAdmin) {
		let newCourse = new Course ({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});

		console.log(newCourse);
		return newCourse.save().then((course, error) => {
			if (error) {
				return false;
			}
			else {
				return true;
			}
		});
	}
	else {
			return false;
	}
};


// Retrieve All Courses
/*
	1. Retrieve all the courses from the database using the find() method
		Model.find({})
*/
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	});
};


// Retrieve All Active Courses
/*	
	STEP:
	1. Retrieve all the courses from the database with the property of "isActive" to true
*/
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	});
};


// Retrieving a Specific Course
/*
	STEP:
	1. Retrieve the course that matches the course ID provided from the URl
*/
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result
	});
};


// Update a Course
/*
	STEPS:
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
	2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
*/
module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if (error) {
			return false;
		}
		else {
			return true;
		}
	});
};


// Archive a course
// In managing databases, it's common practice to soft delete our records and what we would implement in the "delete" operation of our application
// The "soft delete" happens here by simply updating the course "isActive" status into "false" which will no longer be displayed in the frontend application whenever all active courses are retrieved
// This allows us access to these records for future use and hides them away from users in our frontend application
// There are instances where hard deleting records is required to maintain the records and clean our databases
// The use of "hard delete" refers to removing records from our database permanently


/***************** ACTIVITY 40  *****************/ 

// Archive a Course
module.exports.archiveCourse = (reqParams, reqBody) => {
	let archiveCourse = {
		isActive : reqBody.isActive
	};
	return Course.findByIdAndUpdate(reqParams.courseId, archiveCourse).then((course, error) => {
		if (error) {
			return false;
		}
		else {
			return {message : "This course is now inactive."};
		}
	});
};

/***************** SOLUTION 1 ACTIVITY 40  *****************/ 
/*module.exports.archiveCourse = (reqParams) => {
	let updatedActiveField = {
		isActive : false;
	};
	return Course.findByIdAndUpdate(reqParams.courseId, updatedActiveField).then((course, error) => {
		if (error) {
			return false;
		}
		else {
			return true;
		}
	});
};*/


/***************** SOLUTION 2 ACTIVITY 40  *****************/ 
/*module.exports.archiveCourse = (reqParams)=>{
	return Course.findByIdAndUpdate(reqParams,{isActive:false}).then((course,error)=>{
		if(error){
			return false
		}else{
			return true
		}
	});
};*/


//Stretch Goal (To Activate Course)
/*module.exports.activateCourse = (reqParams)=>{
	return Course.findByIdAndUpdate(reqParams,{isActive:true}).then((course,error)=>{
		if(error){
			return false
		}else{
			return true
		}
	});
};*/




