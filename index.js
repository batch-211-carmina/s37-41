const express = require("express");
const mongoose = require("mongoose");
// cors - allows our backend application to be available to our frontend application
// cors - allows us to control the app's Cross origin Resource Sharing settings
const cors = require("cors");

// from routes folder and userRoutes.js
const userRoutes = require("./routes/userRoutes");

// from routes folder and courseRoutes.js
const courseRoutes = require("./routes/courseRoutes");


const app = express();

mongoose.connect("mongodb+srv://admin123:admin123@project0.4hc3b3v.mongodb.net/s37-41?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once('open', () => console.log("Now connected to MongoDB Atlas."));

// Middlewares
// Allows all resources to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// don't forget to define this
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);


app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
});